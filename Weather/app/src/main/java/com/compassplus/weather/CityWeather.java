package com.compassplus.weather;

import android.util.Pair;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CityWeather{

    private String title;
    private long temp;
    private long tempfl;
    private long humidity;
    private String condition;
    private double lat;
    private double lng;

    public CityWeather (String title, double lat, double lng){
        this.title = title;
        this.lat = lat;
        this.lng = lng;
    }

    static Map<String, Pair<String, Integer>> conditionMap = new HashMap<>();

    static {
        conditionMap.put("clear", Pair.create("ясно", R.drawable.clear));
        conditionMap.put("partly-cloudy", Pair.create("малооблачно", R.drawable.partlycloudy));
        conditionMap.put("cloudy", Pair.create("облачно с прояснениями", R.drawable.cloudy));
        conditionMap.put("overcast", Pair.create("пасмурно", R.drawable.overcast));
        conditionMap.put("partly-cloudy-and-light-rain", Pair.create("небольшой дождь", R.drawable.partlycloudyandlightrain));
        conditionMap.put("partly-cloudy-and-rain", Pair.create("дождь", R.drawable.rain2));
        conditionMap.put("overcast-and-rain", Pair.create("сильный дождь", R.drawable.rain));
        conditionMap.put("overcast-thunderstorms-with-rain", Pair.create("гроза", R.drawable.overcastthunderstormswithrain));
        conditionMap.put("cloudy-and-light-rain", Pair.create("небольшой дождь", R.drawable.rain2));
        conditionMap.put("cloudy-and-rain", Pair.create("небольшой дождь", R.drawable.rain));
        conditionMap.put("overcast-and-wet-snow", Pair.create("дождь", R.drawable.rain));
        conditionMap.put("overcast-and-light-rain", Pair.create("дождь со снегом", R.drawable.overcastandlightrain));
        conditionMap.put("partly-cloudy-and-light-snow", Pair.create("небольшой снег", R.drawable.partlycloudyandlightsnow));
        conditionMap.put("cloudy-and-light-snow", Pair.create("небольшой снег", R.drawable.snow));
        conditionMap.put("overcast-and-light-snow", Pair.create("небольшой снег", R.drawable.partlycloudyandlightsnow));
        conditionMap.put("partly-cloudy-and-snow", Pair.create("снег", R.drawable.snowing));
        conditionMap.put("cloudy-and-snow",  Pair.create("снег", R.drawable.snowing));
        conditionMap.put("overcast-and-snow",  Pair.create("снег", R.drawable.snowing));
    }

    public String getConditionString (){
        return conditionMap.get(condition).first;
    }

    public int getIconId(){ return conditionMap.get(condition).second;}

    public String getTitle(){
        return title;
    }

    public long getTemp() {
        return temp;
    }

    public double getLat(){ return lat;}

    public double getLng() { return lng;}

    public long getTempfl() {
        return tempfl;
    }

    public String getCondition() {
        return condition;
    }

    public long getHumidity() {
        return humidity;
    }

    public String doGet() throws IOException {

        String url = "https://api.weather.yandex.ru/v1/forecast?lat=" + getLat() + "&lon=" + getLng() + "&extra=false";

        URL urlObj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();

        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("X-Yandex-API-Key", "8474641f-2cb9-41ce-922b-19fc7b3ca599");
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        connection.setRequestProperty("Content-Type", "application/json");
        int code = connection.getResponseCode();

        if (code == 200){
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = bufferedReader.readLine()) != null) {
                response.append(inputLine);
            }
            bufferedReader.close();

            return response.toString();
        } else return "";
    }

    public void Parse(String response){
        JSONParser jsonParser = new JSONParser();
        JSONObject resObj;
        try {
            resObj = (JSONObject) jsonParser.parse(response);
            JSONObject factObj = (JSONObject) resObj.get("fact");
            temp = (long)factObj.get("temp");
            tempfl = (long)factObj.get("feels_like");
            humidity = (long)factObj.get("humidity");
            condition = (String)factObj.get("condition");
        } catch (ParseException parseExc){
            System.err.println(parseExc.toString());
        }

    }

}
