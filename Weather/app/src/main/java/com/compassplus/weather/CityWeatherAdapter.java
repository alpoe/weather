package com.compassplus.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class CityWeatherAdapter extends ArrayAdapter<CityWeather> {

    private Context mContext;
    private List<CityWeather> cityWeathersList = new ArrayList<>();

    public CityWeatherAdapter(Context context, ArrayList<CityWeather> list) {
        super(context, 0 , list);
        mContext = context;
        cityWeathersList = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_view,parent,false);

        CityWeather currentCityWeather = cityWeathersList.get(position);

        TextView condition = listItem.findViewById(R.id.condition);
        condition.setText(currentCityWeather.getConditionString());

        ImageView icon = listItem.findViewById(R.id.iconWeather);
        icon.setImageDrawable(mContext.getResources().getDrawable(currentCityWeather.getIconId()));

        TextView title = (TextView) listItem.findViewById(R.id.city);
        title.setText(currentCityWeather.getTitle());

        TextView temp = (TextView) listItem.findViewById(R.id.temp);
        String str1 = String.valueOf(currentCityWeather.getTemp()) + mContext.getString(R.string.degree);
        temp.setText(str1);

        TextView tempfl = (TextView) listItem.findViewById(R.id.tempfl);
        String str2 = mContext.getString(R.string.feelsLike)+ " "  + String.valueOf(currentCityWeather.getTempfl()) + mContext.getString(R.string.degree);
        tempfl.setText(str2);

        TextView humidity = (TextView)listItem.findViewById(R.id.humidity);
        String str3 = mContext.getString(R.string.humidity) + " " + String.valueOf(currentCityWeather.getHumidity()) + mContext.getString(R.string.percent);
        humidity.setText(str3);

        return listItem;
    }
}


