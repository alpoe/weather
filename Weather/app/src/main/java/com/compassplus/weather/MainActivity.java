package com.compassplus.weather;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Pair;
import android.widget.ListView;
import android.location.LocationManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity implements LocationListener {
    private ListView listView;
    private CityWeatherAdapter wAdapter;
    protected LocationManager locationManager;
    final long minTime = 0L;
    final long minDistance = 5000L;
    Map<String, Pair<Double, Double>> CityMap = new HashMap<>();
    ArrayList<CityWeather> weatherArrayList = new ArrayList<>();
    Double lat;
    Double lng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!isOnline(MainActivity.this)) buildDialog(MainActivity.this, getString(R.string.noInternetTitle), getString(R.string.noInternetBody)).show();
        else {
            setContentView(R.layout.activity_main);

            listView = (ListView) findViewById(R.id.city_weather_list);

            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            try {locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minDistance, this);}
            catch(SecurityException exc) {
                Log.e("LocationListener", exc.getMessage());
            }

            CityMap.put("Москва", Pair.create(55.753960, 37.620393));
            CityMap.put("Лондон", Pair.create(51.5085300, -0.1257400));
            CityMap.put("Сидней", Pair.create(-33.8678500, 151.2073200));
            CityMap.put("Вашингтон", Pair.create(38.8951100, -77.0363700));
            CityMap.put("Пекин", Pair.create(39.9075000, 116.3972300));

            for (Map.Entry<String, Pair<Double, Double>> entry : CityMap.entrySet()) {
                CityWeather cw = new CityWeather(entry.getKey(), entry.getValue().first, entry.getValue().second);
                DoGetTask doGetTask = new DoGetTask();
                doGetTask.execute(cw);
                try {
                    String resp = doGetTask.get();
                    if (!resp.equals("")) {
                        cw.Parse(resp);
                            weatherArrayList.add(cw);
                    } else {
                        buildDialog(MainActivity.this, getString(R.string.serverErrTitle), getString(R.string.serverErrBody)).show();
                    }
                } catch (Exception exc) {
                    Log.e("doGetTaskInActivity", exc.getMessage());
                }
                wAdapter = new CityWeatherAdapter(this, weatherArrayList);
                listView.setAdapter(wAdapter);
            }
        }

        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
        CityWeather cw = new CityWeather("Текущий регион",lat, lng);
        DoGetTask doGetTask = new DoGetTask();
        doGetTask.execute(cw);
        try {
            String resp = doGetTask.get();
            if (!resp.equals("")) {
                cw.Parse(resp);
                if(weatherArrayList.size() < 6){
                    weatherArrayList.add(cw);
                }
                else {
                    weatherArrayList.set(5, cw);
                }
            } else {
                buildDialog(MainActivity.this, getString(R.string.serverErrTitle), getString(R.string.serverErrBody)).show();
            }
        } catch (Exception exc) {
            Log.e("doGetTaskInActivity", exc.getMessage());
        }
        wAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Location","status");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Location","enable");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Location","disable");
    }

    public boolean isOnline(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni != null && ni.isConnected())
            return true;
        else
            return false;
    }

    public AlertDialog.Builder buildDialog(Context c, String str1, String str2) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle(str1);
        builder.setMessage(str2);

        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });

        return builder;
    }

    class DoGetTask extends AsyncTask<CityWeather, Void, String>{
        @Override
        protected String doInBackground(CityWeather ...objs) {

            try {
                String resp = objs[0].doGet();
                return resp;
            } catch(Exception exc){
                Log.e("DoGetTask", exc.getMessage());
                return null;
            }
        }
    }
}
